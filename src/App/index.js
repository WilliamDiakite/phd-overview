import React from 'react';
import './App.css';
import Graph from './Graph'
import Bib from './bib.json'


function App() {
  return (
    <div className="App">

      <header>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600&display=swap" rel="stylesheet" />
        <h1>Bibliographie Novembre 2019</h1>
        <h2>Smart-ACA</h2>
      </header>

      <div className='container'>
        {
            Bib.map((i, m) => {
              return(
                <div
                  key={`refs-${m}`}
                  className='chapter'
                >
                  <div className='graph-container'>
                    <Graph nodes={i.nodes}/>
                  </div>
                  <div className='content'>
                    <h3>{i.title}</h3>
                    <div className='refs'>
                      {i.refs
                        .sort()
                        .map((r, n) => <div
                                          key={`refs-${m}-${n}`}
                                          className='ref'
                                        >
                                          {r.split('#')[0]}<i>{r.split('#')[1]}</i>
                                        </div>)
                      }
                    </div>
                  </div>
                </div>
              )
            })
        }
      </div>


    </div>
  );
}

export default App;
