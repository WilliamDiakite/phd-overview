import React from "react";
import './App.css'

export default class Graph extends React.Component {
  render() {
    return (
      <svg
        className='graph-small'
        xmlns="http://www.w3.org/2000/svg"
  
        viewBox="-504.335 -311.497 1177.961 830.567"
      >
        <g className="layer relationships">
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h112.365v-1H25zm166.529 0h112.365v3l7-3.5-7-3.5v3H191.529z"
              className="outline"
              transform="rotate(-62.198 316.902 16.8)"
            ></path>
            <text
              x="164.447"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-62.198 316.902 16.8)"
            >
              IS_PART_OF
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h55.821v-1H25zm122.723 0h55.821v3l7-3.5-7-3.5v3h-55.82z"
              className="outline"
              transform="rotate(179.647 76.67 144.88)"
            ></path>
            <text
              x="114.272"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(179.647 76.67 144.88) rotate(180 114.272 0)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h55.716v-1H25zm122.619 0h55.716v3l7-3.5-7-3.5v3H147.62z"
              className="outline"
              transform="rotate(163.206 153.39 97.784)"
            ></path>
            <text
              x="114.168"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(163.206 153.39 97.784) rotate(180 114.168 0)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h17.56v-1H25zm81.349 0h17.56v3l7-3.5-7-3.5v3h-17.56z"
              className="outline"
              transform="rotate(-96.478 229.906 -73.11)"
            ></path>
            <text
              x="74.454"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-96.478 229.906 -73.11) rotate(180 74.454 0)"
            >
              EMBEDDED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M24.019-6.953l.259.965A720.837 720.837 0 01181.489-29.95l-.04-1A721.837 721.837 0 0024.018-6.953zm210.133-24.22l-.032 1A720.837 720.837 0 01391.528-7.538l-.752 2.904 7.654-1.633-5.899-5.143-.752 2.904a721.837 721.837 0 00-157.627-22.668"
              className="outline"
              transform="rotate(-85.051 391.69 -107.669)"
            ></path>
            <text
              x="207.801"
              y="-28.043"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-85.051 391.69 -107.669)"
            >
              TESTED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M24.019-6.953l.259.965A504.428 504.428 0 01120.12-21.98l-.069-.997A505.428 505.428 0 0024.02-6.953zM183.86-23.341l-.057.998a504.428 504.428 0 0196.02 14.898l-.744 2.907 7.65-1.657-5.915-5.125-.744 2.906a505.428 505.428 0 00-96.21-14.927"
              className="outline"
              transform="rotate(-179.527 233.186 144.965)"
            ></path>
            <text
              x="151.954"
              y="-20.667"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-179.527 233.186 144.965) rotate(180 151.954 -23.667)"
            >
              EMBEDDED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M24.019 6.953l.259-.965a720.837 720.837 0 00151.666 23.716l-.049.999A721.837 721.837 0 0124.02 6.953zm215.688 24.02l-.04-1A720.837 720.837 0 00391.528 7.537l-.752-2.904 7.654 1.633-5.899 5.143-.752-2.904a721.837 721.837 0 01-152.072 22.467"
              className="outline"
              transform="rotate(-85.051 391.69 -107.669)"
            ></path>
            <text
              x="207.801"
              y="34.043"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-85.051 391.69 -107.669)"
            >
              EMBEDDED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h123.064v-1H25zm189.967 0H338.03v3l7-3.5-7-3.5v3H214.967z"
              className="outline"
              transform="rotate(-168.025 247.89 121.53)"
            ></path>
            <text
              x="181.515"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-168.025 247.89 121.53) rotate(180 181.515 0)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M24.019 6.953l.259-.965A504.428 504.428 0 00125.659 22.33l-.058.998A505.428 505.428 0 0124.02 6.953zm154.29 16.677l-.047-.999A504.428 504.428 0 00279.824 7.445l-.744-2.907 7.65 1.657-5.915 5.125-.744-2.906A505.428 505.428 0 01178.308 23.63"
              className="outline"
              transform="rotate(-179.527 233.186 144.965)"
            ></path>
            <text
              x="151.954"
              y="26.667"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-179.527 233.186 144.965) rotate(180 151.954 23.667)"
            >
              TESTED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h39.154v-1H25zm102.943 0h39.154v3l7-3.5-7-3.5v3h-39.154z"
              className="outline"
              transform="rotate(-133.353 295.5 45.646)"
            ></path>
            <text
              x="96.049"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-133.353 295.5 45.646) rotate(180 96.049 0)"
            >
              EMBEDDED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h57.454v-1H25zm111.618 0h57.454v3l7-3.5-7-3.5v3h-57.454z"
              className="outline"
              transform="rotate(147.529 269.626 8.434)"
            ></path>
            <text
              x="109.536"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(147.529 269.626 8.434) rotate(180 109.536 0)"
            >
              IS_PART_OF
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h10.127v-1H25zm77.029 0h10.126v3l7-3.5-7-3.5v3H102.03z"
              className="outline"
              transform="rotate(7.263 1268.719 3887.253)"
            ></path>
            <text
              x="68.578"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(7.263 1268.719 3887.253)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h31.954v-1H25zm108.189 0h31.954v3l7-3.5-7-3.5v3H133.19z"
              className="outline"
              transform="rotate(47.993 369.758 -23.405)"
            ></path>
            <text
              x="95.072"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(47.993 369.758 -23.405)"
            >
              IMPLEMENTED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h7.684v-1H25zm83.919 0h7.684v3l7-3.5-7-3.5v3h-7.684z"
              className="outline"
              transform="rotate(94.687 182.6 -92.915)"
            ></path>
            <text
              x="70.802"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(94.687 182.6 -92.915) rotate(180 70.802 0)"
            >
              IMPLEMENTED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h37.703v-1H25zm101.492 0h37.702v3l7-3.5-7-3.5v3h-37.702z"
              className="outline"
              transform="rotate(142.121 100.928 -123.248)"
            ></path>
            <text
              x="94.597"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(142.121 100.928 -123.248) rotate(180 94.597 0)"
            >
              EMBEDDED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h73.796v-1H25zm127.96 0h73.797v3l7-3.5-7-3.5v3h-73.796z"
              className="outline"
              transform="rotate(36.835 218.333 -156.005)"
            ></path>
            <text
              x="125.878"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(36.835 218.333 -156.005)"
            >
              IS_PART_OF
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h98.77v-1H25zm166.566 0h98.77v3l7-3.5-7-3.5v3h-98.77z"
              className="outline"
              transform="rotate(103.624 38.757 -100.66)"
            ></path>
            <text
              x="157.668"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(103.624 38.757 -100.66) rotate(180 157.668 0)"
            >
              DESCRIBED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h41.703v-1H25zm100.156 0h41.703v3l7-3.5-7-3.5v3h-41.703z"
              className="outline"
              transform="rotate(154.095 -80.324 61.14)"
            ></path>
            <text
              x="95.93"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(154.095 -80.324 61.14) rotate(180 95.93 0)"
            >
              ENCODED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h8.055v-1H25zm59.696 0h8.054v3l7-3.5-7-3.5v3h-8.054z"
              className="outline"
              transform="rotate(177.329 -175.44 -.654)"
            ></path>
            <text
              x="58.875"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(177.329 -175.44 -.654) rotate(180 58.875 0)"
            >
              STORED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h6.862v-1H25zm61.026 0h6.861v3l7-3.5-7-3.5v3h-6.861z"
              className="outline"
              transform="rotate(126.243 -177.1 -85.448)"
            ></path>
            <text
              x="58.944"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(126.243 -177.1 -85.448) rotate(180 58.944 0)"
            >
              IS_PART_OF
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h10.45v-1H25zm91.584 0h10.45v3l7-3.5-7-3.5v3h-10.45z"
              className="outline"
              transform="rotate(-37.64 -165.284 517.967)"
            ></path>
            <text
              x="76.017"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-37.64 -165.284 517.967)"
            >
              REPRESENTED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h24.592v-1H25zm105.725 0h24.592v3l7-3.5-7-3.5v3h-24.592z"
              className="outline"
              transform="rotate(24.849 -190.947 -792.517)"
            ></path>
            <text
              x="90.158"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(24.849 -190.947 -792.517)"
            >
              REPRESENTED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h14.917v-1H25zm81.824 0h14.917v3l7-3.5-7-3.5v3h-14.917z"
              className="outline"
              transform="rotate(-.345 -1069.022 -26108.619)"
            ></path>
            <text
              x="73.371"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-.345 -1069.022 -26108.619)"
            >
              INSPECTED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h52.273v-1H25zm119.176 0h52.273v3l7-3.5-7-3.5v3h-52.273z"
              className="outline"
              transform="rotate(103.665 81.296 58.31)"
            ></path>
            <text
              x="110.724"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(103.665 81.296 58.31) rotate(180 110.724 0)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h12.717v-1H25zm79.62 0h12.717v3l7-3.5-7-3.5v3H104.62z"
              className="outline"
              transform="rotate(-179.099 78.553 -4.072)"
            ></path>
            <text
              x="71.168"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-179.099 78.553 -4.072) rotate(180 71.168 0)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h99.706v-1H25zm167.503 0h99.706v3l7-3.5-7-3.5v3h-99.706z"
              className="outline"
              transform="rotate(150.806 79.48 17.01)"
            ></path>
            <text
              x="158.605"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(150.806 79.48 17.01) rotate(180 158.605 0)"
            >
              DESCRIBED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h21.536v-1H25zm73.176 0h21.536v3l7-3.5-7-3.5v3H98.176z"
              className="outline"
              transform="rotate(-58.314 72.388 -144.306)"
            ></path>
            <text
              x="72.356"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-58.314 72.388 -144.306)"
            >
              STORED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h17.089v-1H25zm68.73 0h17.089v3l7-3.5-7-3.5v3h-17.09z"
              className="outline"
              transform="rotate(-116.797 76.455 -51.8)"
            ></path>
            <text
              x="67.909"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-116.797 76.455 -51.8) rotate(180 67.91 0)"
            >
              STORED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h40.557v-1H25zm99.01 0h40.558v3l7-3.5-7-3.5v3H124.01z"
              className="outline"
              transform="rotate(129.11 -110.733 -.207)"
            ></path>
            <text
              x="94.784"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(129.11 -110.733 -.207) rotate(180 94.784 0)"
            >
              ENCODED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h84.441v-1H25zm138.605 0h84.441v3l7-3.5-7-3.5v3h-84.44z"
              className="outline"
              transform="rotate(-62.155 -19.372 192.739)"
            ></path>
            <text
              x="136.523"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-62.155 -19.372 192.739)"
            >
              IS_PART_OF
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h43.595v-1H25zm110.498 0h43.595v3l7-3.5-7-3.5v3h-43.595z"
              className="outline"
              transform="rotate(-26.698 89.963 423.644)"
            ></path>
            <text
              x="102.047"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-26.698 89.963 423.644)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h111.252v-1H25zm169.705 0h111.252v3l7-3.5-7-3.5v3H194.705z"
              className="outline"
              transform="rotate(103.685 -76.936 -133.436)"
            ></path>
            <text
              x="165.478"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(103.685 -76.936 -133.436) rotate(180 165.478 0)"
            >
              ENCODED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h38.921v-1H25zm93.085 0h38.922v3l7-3.5-7-3.5v3h-38.922z"
              className="outline"
              transform="rotate(-22.321 -341.131 524.571)"
            ></path>
            <text
              x="91.003"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-22.321 -341.131 524.571)"
            >
              IS_PART_OF
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h61.212v-1H25zm128.114 0h61.212v3l7-3.5-7-3.5v3h-61.212z"
              className="outline"
              transform="rotate(19.197 154.452 -709.731)"
            ></path>
            <text
              x="119.663"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(19.197 154.452 -709.731)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h17.291v-1H25zm84.193 0h17.292v3l7-3.5-7-3.5v3h-17.292z"
              className="outline"
              transform="rotate(5.859 -1970.588 -432.723)"
            ></path>
            <text
              x="75.742"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(5.859 -1970.588 -432.723)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h46.688v-1H25zm113.59 0h46.687v3l7-3.5-7-3.5v3H138.59z"
              className="outline"
              transform="rotate(-73.335 106.343 136.026)"
            ></path>
            <text
              x="105.139"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-73.335 106.343 136.026)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h87.204v-1H25zm150.993 0h87.204v3l7-3.5-7-3.5v3h-87.204z"
              className="outline"
              transform="rotate(-44.201 217.672 166.513)"
            ></path>
            <text
              x="144.098"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(-44.201 217.672 166.513)"
            >
              EMBEDDED_IN
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h52.822v-1H25zm119.725 0h52.822v3l7-3.5-7-3.5v3h-52.822z"
              className="outline"
              transform="rotate(13.254 -1177.134 -1192.487)"
            ></path>
            <text
              x="111.274"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(13.254 -1177.134 -1192.487)"
            >
              PRODUCED_BY
            </text>
          </g>
          <g className="relationship">
            <path
              fill="#A5ABB6"
              d="M25 .5h92.608v-1H25zm155.073 0h92.608v3l7-3.5-7-3.5v3h-92.608z"
              className="outline"
              transform="rotate(132.978 157.154 63.709)"
            ></path>
            <text
              x="148.841"
              y="3"
              fontSize="8"
              pointerEvents="none"
              textAnchor="middle"
              transform="rotate(132.978 157.154 63.709) rotate(180 148.84 0)"
            >
              EQUIPPED_BY
            </text>
          </g>
        </g>
        <g className="layer nodes">
          <g className="node" transform="translate(7.842 -9.256)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Archivist') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Archivist
            </text>
          </g>
          <g className="node" transform="translate(-475.335 12.681)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Institution') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Institution
            </text>
          </g>
          <g className="node" transform="translate(103.19 215.073)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Scholar') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Scholar
            </text>
          </g>
          <g className="node" transform="translate(644.626 -110.982)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Designer') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Designer
            </text>
          </g>
          <g className="node" transform="translate(-81.31 290.735)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Engineer') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Engineer
            </text>
          </g>
          <g className="node" transform="translate(92.927 490.07)">
            <circle
              r="29"
              fill={this.props.nodes.includes('HumaNum') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              HumaNum
            </text>
          </g>
          <g className="node" transform="translate(290.636 430.747)">
            <circle
              r="29"
              fill={this.props.nodes.includes('MSH') ? "#57C7E3" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              MSH
            </text>
          </g>
          <g className="node" transform="translate(154.23 289.284)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Algorithm') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Algorithm
            </text>
          </g>
          <g className="node" transform="translate(489.87 -267.128)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Interface') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Interface
            </text>
          </g>
          <g className="node" transform="translate(328.49 147.079)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Theory') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Theory
            </text>
          </g>
          <g className="node" transform="translate(465.17 291.85)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Hypothesis') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Hypothesis
            </text>
          </g>
          <g className="node" transform="translate(501.627 -129.207)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Visualization') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Visualization
            </text>
          </g>
          <g className="node" transform="translate(92.775 -134.39)">
            <circle
              r="29"
              fill={this.props.nodes.includes('LDatabase') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              LDatabase
            </text>
          </g>
          <g className="node" transform="translate(236.85 -136.007)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Database') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Database
            </text>
          </g>
          <g className="node" transform="translate(104.918 -282.497)">
            <circle
              r="29"
              fill={this.props.nodes.includes('FOntologie') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              FOntologie
            </text>
          </g>
          <g className="node" transform="translate(-49.94 -162.036)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Data') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Data
            </text>
          </g>
          <g className="node" transform="translate(-125.865 151.23)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Metadata') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Metadata
            </text>
          </g>
          <g className="node" transform="translate(250.864 -277.303)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Code') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Code
            </text>
          </g>
          <g className="node" transform="translate(-350.72 6.868)">
            <circle
              r="29"
              fill={this.props.nodes.includes('PDocument') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              PDocument
            </text>
          </g>
          <g className="node" transform="translate(-424.555 107.592)">
            <circle
              r="29"
              fill={this.props.nodes.includes('PCollection') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              PCollection
            </text>
          </g>
          <g className="node" transform="translate(157.16 -6.909)">
            <circle
              r="29"
              fill={this.props.nodes.includes('DCollection') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              DCollection
            </text>
          </g>
          <g className="node" transform="translate(-180.746 85.585)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Scan') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Scan
            </text>
          </g>
          <g className="node" transform="translate(-224.786 -90.253)">
            <circle
              r="29"
              fill={this.props.nodes.includes('OCR') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              OCR
            </text>
          </g>
          <g className="node" transform="translate(-54.466 198.895)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Classification') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Classification
            </text>
          </g>
          <g className="node" transform="translate(-304.743 238.109)">
            <circle
              r="29"
              fill={this.props.nodes.includes('FFormat') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              FFormat
            </text>
          </g>
          <g className="node" transform="translate(310.899 -7.834)">
            <circle
              r="29"
              fill={this.props.nodes.includes('Tool') ? "#F16667" : 'grey'}
              stroke="black"
              strokeWidth="0.5"
              className="outline"
            ></circle>
            <text
              y="5"
              fill="#FFF"
              fontSize="10"
              pointerEvents="none"
              textAnchor="middle"
            >
              Tool
            </text>
          </g>
        </g>
      </svg>
    );
  }
}
